<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index()
    {
        return view('login/index', ['title' => 'Login',]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            // $request->session()->flash('successLogin', 'Login successfully!');
            // Session::flash('successLogin', 'Login successfully!');
            return redirect()->intended('/dashboard')->with('successLogin', 'Login successfully!');
        }

        return back()->with(
            'loginError',
            'Login Failed! Incorrect an email/password',
        );
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/')->with('successLogout', 'Logout successfully!');
    }
}
